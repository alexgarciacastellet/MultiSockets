/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package chats;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author alex
 */
public class chat_server extends javax.swing.JFrame {

    public String st = "ssss";
    Map<Integer, Socket> chat_list = new HashMap<Integer, Socket>();
    String llista = "";
    static final int PORT = 1978;

    public static void main(String args[]) {
        chat_server cs = new chat_server();
        cs.init();
    }

    public void init() {
        ServerSocket serverSocket = null;
        Socket socket = null;
        System.out.println("INICI... ");

        try {
            serverSocket = new ServerSocket(PORT);
        } catch (IOException e) {
            e.printStackTrace();
        }

        while (true) {
            try {
                socket = serverSocket.accept();/*****************************************************/
               // comunica(0, socket.getPort(), "Estàs connectat amb el port: "+socket.getPort());
            } catch (IOException e) {
                System.out.println("I/O error: " + e);
            }

            chat_list.put(socket.getPort(), socket);
            new EchoThread2(socket, this).start();

            llista = "" + creaLlista(socket.getPort());

            comunica(0, 0, llista);

        }
    }

    public void comunica(int orig, int port, String mensa) {
        DataOutputStream dout;

        if (port == 0) {
            chat_list.forEach((k, v) -> {
                try {
                    DataOutputStream dout2;
                    String server = "";
                    String text;

                        if (orig==0) text=creaLlista(k);
                        else text=mensa;
                    dout2 = new DataOutputStream(v.getOutputStream());
                    dout2.writeUTF(orig + " per tothom> " + text);
                } catch (IOException ex) {
                    Logger.getLogger(chat_server.class.getName()).log(Level.SEVERE, null, ex);
                }

            });
        } else {
            Socket socket = chat_list.get(port);
            try {
                dout = new DataOutputStream(socket.getOutputStream());
                dout.writeUTF(orig + " diu: " + mensa);
            } catch (IOException ex) {
                Logger.getLogger(chat_server.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    private String creaLlista(int ego) {
        System.out.println(ego);
        
        llista=" Connectats: ";
        chat_list.forEach((k, v) -> {
           // System.out.println("++++++"+ego+k);
            String marca=(ego==k?"*":"");
            llista = llista + "\n "+ marca + k+"@";
        });
        return llista;
    }
}

class EchoThread2 extends Thread {

    public Socket socket;
    public chat_server papi;

    public EchoThread2(Socket clientSocket, chat_server papi) {
        this.socket = clientSocket;
        this.papi = papi;
    }

    public void run() {
        DataOutputStream out = null;
        DataInputStream in = null;

        System.out.println("conexió: " + socket.toString() + " *** " + socket.getPort());
        try {
            in = new DataInputStream(socket.getInputStream());
            out = new DataOutputStream(socket.getOutputStream());
        } catch (IOException e) {
            return;
        }
        String line;

        while (true) {
            try {
                line = in.readUTF();
                if ((line == null) || line.equalsIgnoreCase("QUIT")) {
                    socket.close();
                    return;
                } else {
                    out.writeUTF(">>>>>>>>>>>>TU: " + line + "\n\r");
                    out.flush();
                    System.out.println("REBUT>>" + line);
                    String[] exploded = new String[2];
                    exploded[0] = "0";
                    exploded[1] = line;
                    //  System.out.println(  "....." + line );
                    if (line.contains("@")) {
                        exploded = line.split("@");
                        System.out.println("");
                    }
                    papi.comunica(socket.getPort(), Integer.parseInt(exploded[0]), exploded[1]);
                }
            } catch (IOException e) {
                e.printStackTrace();
                return;
            }
        }
    }
}
